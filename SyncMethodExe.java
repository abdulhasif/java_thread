class DisplayClass
{
    synchronized void printData(String name)
    {
        for(int inc=0;inc<2;inc++)
        {
            System.out.println("Hello "+ name);
            
        }
    }
}
class ThreadTwo extends Thread
{
    DisplayClass dc;
    ThreadTwo(DisplayClass dc)
    {
        this.dc = dc;
    }

    public void run()
    {
        dc.printData("Hasif");
    }
}
class ThreadOne extends Thread
{
    DisplayClass dc;
    ThreadOne(DisplayClass dc)
    {
        this.dc = dc;
    }

    public void run()
    {
        dc.printData("Abdul");
    }
}
public class SyncMethodExe 
{
    public static void main(String args[])
    {
        DisplayClass dc = new DisplayClass();
        ThreadOne t1 = new ThreadOne(dc);
        ThreadTwo t2 = new ThreadTwo(dc);
        t1.setPriority(5);
        t2.setPriority(1);
        t1.start();
        t2.start();
    }
}