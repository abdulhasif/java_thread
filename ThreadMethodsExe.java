public class ThreadMethodsExe extends Thread
{
    private String threadName;
    
    public void run()
    {
        for(int inc=1;inc<=2;inc++)
        {
            try
            {
                System.out.println("Thread "+ threadName + " is running Test-"+inc );
                Thread.sleep(1000); //makes thead to sleep for specific amount of time  
            } 
            catch (InterruptedException e) 
            {
                System.out.println("Interrupted with exception" + e);
            }
            
        }
        System.out.println(Thread.currentThread()); //returns the current thread running
    }

    public static void main(String args[])
    {
        ThreadMethodsExe thread1 = new ThreadMethodsExe();
        ThreadMethodsExe thread2 = new ThreadMethodsExe();
        thread1.threadName = "Hola";
        thread2.threadName = "Register";

        thread1.start();
        thread2.start();
        thread1.setPriority(2);
        thread2.setPriority(1); //set the priority for the thread
        System.out.println(Thread.currentThread());
        System.out.println(thread1.getPriority()); // get the priority of the thread
        System.out.println("Id : "+thread2.getId()); //get the ID of the thread
    }
}