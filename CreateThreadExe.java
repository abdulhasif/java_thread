
//Creating thread using Thread class
// public class CreateThreadExe extends Thread
// {
//     public void run()
//     {
//         System.out.println("Thread is running");
//     }

//     public static void main(String args[])
//     {
//         CreateThreadExe thread = new CreateThreadExe();
//         thread.start();
//     }

// }

//Creating thread using Runnable interface
public class CreateThreadExe implements Runnable
{
    public void run()
    {
        System.out.println("Thread is running");
    }

    public static void main(String args[])
    {
        CreateThreadExe createThreadExe = new CreateThreadExe();
        Thread thread = new Thread(createThreadExe);
        thread.start();
    }
}