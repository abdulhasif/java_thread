public class MultithreadingExe extends Thread
{
    private String threadName;
    
    public void run()
    {
        for(int inc=1;inc<=5;inc++)
        {
            try
            {
                System.out.println("Thread "+ threadName + " is running Test-"+inc );
                Thread.sleep(1000);    
            } 
            catch (InterruptedException e) 
            {
                System.out.println("Interrupted with exception" + e);
            }
            
        }
    }

    public static void main(String args[])
    {
        MultithreadingExe thread1 = new MultithreadingExe();
        MultithreadingExe thread2 = new MultithreadingExe();
        thread1.threadName = "Hola";
        thread2.threadName = "Register";

        thread1.start();
        thread2.start();
        
    }
}