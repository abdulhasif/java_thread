class DisplayClass
{
    synchronized static void printData(String name)
    {
        for(int inc=0;inc<2;inc++)
        {
            System.out.println("Hello "+ name);
        }
    }
}
class ThreadTwo extends Thread
{
    public void run()
    {
        DisplayClass.printData("Hasif");
    }
}
class ThreadOne extends Thread
{
    public void run()
    {
        DisplayClass.printData("Abdul");
    }
}
public class SyncBlockExe 
{
    public static void main(String args[])
    {
        ThreadOne t1 = new ThreadOne();
        ThreadTwo t2 = new ThreadTwo();
        t1.start();
        t2.start();
    }
}