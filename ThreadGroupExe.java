public class ThreadGroupExe implements Runnable
{
    public void run()
    {
        
    }
    public static void main(String args[])
    {
        ThreadGroup tgp = new ThreadGroup("Parent Group");
        ThreadGroupExe tg = new ThreadGroupExe();
        Thread t1 = new Thread(tgp,"First");
        Thread t2 = new Thread(tgp,"Second");
        t1.start();
        t2.start();
        System.out.println(tgp.activeCount());
    }
}